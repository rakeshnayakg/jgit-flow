package com.atlassian.maven.plugins.jgitflow.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.Set;

import com.atlassian.maven.plugins.jgitflow.ReleaseContext;
import com.atlassian.maven.plugins.jgitflow.VersionType;
import com.atlassian.maven.plugins.jgitflow.exception.MavenJGitFlowException;
import com.atlassian.maven.plugins.jgitflow.exception.ProjectRewriteException;
import com.atlassian.maven.plugins.jgitflow.provider.ContextProvider;
import com.atlassian.maven.plugins.jgitflow.provider.ProjectCacheKey;
import com.atlassian.maven.plugins.jgitflow.provider.VersionProvider;
import com.atlassian.maven.plugins.jgitflow.rewrite.ProjectChangeset;
import com.atlassian.maven.plugins.jgitflow.rewrite.ProjectRewriter;
import com.google.common.base.Function;
import com.google.common.collect.Maps;

import org.apache.commons.lang.StringUtils;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.component.annotations.Component;
import org.codehaus.plexus.component.annotations.Requirement;
import org.codehaus.plexus.logging.AbstractLogEnabled;

import static com.atlassian.maven.plugins.jgitflow.rewrite.ArtifactReleaseVersionChange.artifactReleaseVersionChange;
import static com.atlassian.maven.plugins.jgitflow.rewrite.ParentReleaseVersionChange.parentReleaseVersionChange;
import static com.atlassian.maven.plugins.jgitflow.rewrite.ProjectReleaseVersionChange.projectReleaseVersionChange;


public class CustomPomUpdater extends DefaultPomUpdater implements PomUpdater
{
  

    
    @Override
    public void addFeatureVersionToSnapshotVersions(ProjectCacheKey cacheKey, final String featureVersion, List<MavenProject> reactorProjects) throws MavenJGitFlowException
    {
    	
        String fullBranchName = branchHelper.getCurrentBranchName();
        
        getLogger().info("RAKESH (" + fullBranchName + ") adding feature versions to poms...");
        ReleaseContext ctx = contextProvider.getContext();
        
        Map<String, String> originalProperty = versionProvider.getOriginalProperty(cacheKey, reactorProjects, "branch.version" );
        
        
        getLogger().info("RAKESH originalProperty=" + originalProperty );
        

        Map<String, String> featureSuffixedProperty = Maps.transformValues(originalProperty, new Function<String, String>()
        {
            @Override
            public String apply(String input)
            {
                if (input.endsWith("-SNAPSHOT"))
                {
                    return StringUtils.substringBeforeLast(input, "-SNAPSHOT") + "-" + featureVersion + "-SNAPSHOT";
                }
                else
                {
                    return input;
                }
            }
        });
        
        getLogger().info("RAKESH featureSuffixedProperty=" + featureSuffixedProperty );

        doUpdate(reactorProjects, originalProperty, featureSuffixedProperty, ctx.isUpdateDependencies());
    }

	

    
}
