package com.atlassian.maven.plugins.jgitflow.provider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.maven.artifact.ArtifactUtils;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.component.annotations.Component;
import org.codehaus.plexus.component.annotations.Requirement;
import org.codehaus.plexus.logging.AbstractLogEnabled;

import com.atlassian.maven.plugins.jgitflow.PrettyPrompter;
import com.atlassian.maven.plugins.jgitflow.helper.MavenExecutionHelper;
import com.google.common.collect.ImmutableMap;

@Component(role = PropertyProvider.class)
public class DefaultPropertyProvider extends AbstractLogEnabled implements PropertyProvider
{
   
    private final Map<ProjectCacheKey, Map<String, Map<String, String>>> originalProperties;    

    @Requirement
    private PrettyPrompter prompter;

    @Requirement
    private MavenExecutionHelper mavenHelper;

    @Requirement
    private JGitFlowProvider jGitFlowProvider;

    @Requirement
    private ContextProvider contextProvider;

    @Requirement
    private MavenSessionProvider sessionProvider;

    public DefaultPropertyProvider()
    {
        
        this.originalProperties = new HashMap<ProjectCacheKey, Map<String, Map<String, String>>>();
    } 
    
    public Map<String, String> getOriginalProperty( ProjectCacheKey cacheKey, List<MavenProject> reactorProjects, String propertNameString )
    {
       
        
            Map<String, String> properties = new HashMap<String, String>();
            for (MavenProject project : reactorProjects)
            {          
            	
            			properties.put(ArtifactUtils.versionlessKey(project.getGroupId(), project.getArtifactId()), project.getProperties().getProperty(propertNameString));
            }

        return ImmutableMap.copyOf(properties);
    }
    
    @Override
    public Map<String, Map<String, String>> getOriginalProperties(ProjectCacheKey cacheKey, List<MavenProject> reactorProjects, List<String> propertyNames)
    {
        if (!originalProperties.containsKey(cacheKey))
        {
        	Map<String, Map<String, String>> properties = new HashMap<String, Map<String, String>>();
            for (MavenProject project : reactorProjects)
            {          
            	Map map =  project.getProperties() ;
            	
            			properties.put(ArtifactUtils.versionlessKey(project.getGroupId(), project.getArtifactId()), map);
            }

            originalProperties.put(cacheKey, properties);
        }

        return ImmutableMap.copyOf(originalProperties.get(cacheKey));
    }

  
    
    public Map<String, Properties> getOriginalProperties(List<MavenProject> reactorProjects, List<String> propertyNames )
    {
    	 Map<String, Properties> properties = new HashMap<String, Properties>();

        for (MavenProject project : reactorProjects)
        {
        	properties.put(ArtifactUtils.versionlessKey(project.getGroupId(), project.getArtifactId()), project.getProperties());
        }

        return ImmutableMap.copyOf(properties);
    }
    
    

   
}
