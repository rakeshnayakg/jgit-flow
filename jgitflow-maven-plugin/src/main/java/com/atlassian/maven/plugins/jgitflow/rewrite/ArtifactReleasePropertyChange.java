package com.atlassian.maven.plugins.jgitflow.rewrite;

import static com.atlassian.maven.plugins.jgitflow.rewrite.ProjectChangeUtils.getElementListOrEmpty;
import static com.atlassian.maven.plugins.jgitflow.rewrite.ProjectChangeUtils.getNamespaceOrNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.maven.artifact.ArtifactUtils;
import org.apache.maven.model.Model;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.logging.AbstractLogEnabled;
import org.jdom2.Element;
import org.jdom2.Namespace;

import com.atlassian.maven.plugins.jgitflow.exception.ProjectRewriteException;

/**
 * @since version
 */
public class ArtifactReleasePropertyChange extends AbstractLogEnabled implements
		ProjectChange {
	private final Map<String, Map<String, String>> originalProperties;
	private final Map<String, Map<String, String>> releaseProperties;
	private final List<String> workLog;
	private final List<String> propertyNames;

	private ArtifactReleasePropertyChange(
			Map<String, Map<String, String>> originalProperties,
			Map<String, Map<String, String>> releaseProperties,
			List<String> propertyNames) {
		this.originalProperties = originalProperties;
		this.releaseProperties = releaseProperties;
		this.workLog = new ArrayList<String>();
		this.propertyNames = propertyNames;
	}

	public static ArtifactReleasePropertyChange artifactReleasePropertyChange(
			Map<String, Map<String, String>> originalVersions,
			Map<String, Map<String, String>> releaseVersions,
			List<String> propertyNames) {
		return new ArtifactReleasePropertyChange(originalVersions,
				releaseVersions, propertyNames);
	}

	@Override
	public boolean applyChange(MavenProject project, Element root)
			throws ProjectRewriteException {
		boolean modified = false;

		if (originalProperties == null || originalProperties.isEmpty()) {
			return modified;
		}

		if (releaseProperties == null || releaseProperties.isEmpty()) {
			return modified;
		}

		if (propertyNames == null || propertyNames.isEmpty()) {
			return modified;
		}

		Properties props = project.getProperties();
		if (!props.isEmpty()) {

		}
		
		

		Namespace ns = getNamespaceOrNull(root);
		Element properties = root.getChild("properties", ns);
		List<Element> artifactContainers = new ArrayList<Element>();
		artifactContainers.add(root);

		artifactContainers.addAll(getElementListOrEmpty(root,
				"profiles/profile", ns));

		for (Element artifactContainer : artifactContainers) {

			modified |= rewriteProperties(artifactContainer, project,
					properties, ns);
		}

		return modified; // To change body of implemented methods use File |
							// Settings | File Templates.
	}

	private boolean rewriteProperties(Element artifactContainer,
			MavenProject project, Element properties, Namespace ns)
			throws ProjectRewriteException {
		List<Element> artifacts = getElementListOrEmpty(artifactContainer,
				"properties", ns);
		return rewriteArtifactVersions(artifacts, project, properties, ns);
	}

	private boolean rewriteArtifactVersions(List<Element> artifacts,
			MavenProject project, Element properties, Namespace ns)
			throws ProjectRewriteException {
		boolean modified = false;
		Model projectModel = project.getModel();

		if (artifacts != null)
			System.out.println("RAKESH artifacts=" + artifacts);

		if (project != null)
			System.out.println("RAKESH project=" + project);

		if (properties != null)
			System.out.println("RAKESH properties=" + properties);

		if (ns != null)
			System.out.println("RAKESH ns=" + ns);

		if (null == artifacts || artifacts.isEmpty()) {
			return modified;
		}

		String artifactKey = ArtifactUtils.versionlessKey(project.getGroupId(),
				project.getArtifactId());

		Map<String, String> mappedVersion = releaseProperties.get(artifactKey);
		Map<String, String> original = originalProperties.get(artifactKey);

		if (null == mappedVersion || null == original
				|| mappedVersion.isEmpty() || original.isEmpty()){
			return modified;
		}

		for (String element : propertyNames) {
			
			String origValue = original.get(element);
			String mappValue = mappedVersion.get(element);
			if ( properties.getChild(element, ns) != null ) {
				//if ( origValue.equals("")  || properties.getChild(element, ns).equals(origValue) ) {
					properties.getChild(element, ns).setText(mappValue);
					modified = true;
				//}
			}else {
				/*
				Element childElement = properties.addContent(element );
				childElement.setText(mappValue);	
				modified = true;
				*/
			}
			
			StringBuilder buff = new StringBuilder("\r\n").append("updating ")
					.append(artifactKey).append(element).append(" to ")
					.append(mappValue);
			System.out.print(buff);
			
		}

		return modified;
	}

	@Override
	public String toString() {
		return workLog.toString();
	}
}
