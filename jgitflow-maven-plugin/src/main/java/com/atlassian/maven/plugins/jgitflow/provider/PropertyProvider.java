package com.atlassian.maven.plugins.jgitflow.provider;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.atlassian.maven.plugins.jgitflow.VersionType;
import com.atlassian.maven.plugins.jgitflow.exception.MavenJGitFlowException;

import org.apache.maven.project.MavenProject;

/**
 * Helper class for getting a map that contains module -> version strings.
 * This is used to get versions for all projects/submodule in a maven reacotr list
 */
public interface PropertyProvider
{
  

	Map<String, Map<String, String>> getOriginalProperties(ProjectCacheKey cacheKey,
			List<MavenProject> reactorProjects, List<String> propertyNames);
    
}
